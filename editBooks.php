<html>
<head>
    <link rel="stylesheet" href="css/datatables.min.css">
</head>
<body>

  <?php 
    include 'navbar_Admin.php';

    $apiKey = 'Z5EGkpadDXuof73_eCtBo5r68HygNdhj';
    $url = 'https://api.mlab.com/api/1/databases/libros/collections/book?apiKey=Z5EGkpadDXuof73_eCtBo5r68HygNdhj';
    $json = file_get_contents($url);
    $books = json_decode($json);

    if(!empty($_POST)){
      $id = $_POST['id'];
      $editBook = array (
        'title' => $_POST['title'],
        'ISBN' => $_POST['ISBN'],
        'volume' => $_POST['volume'],
        'totalVolumes' => $_POST['totalVolumes'],
        'publisherName' => $_POST['publisherName'],
        'yearPublished' => $_POST['yearPublished'],
        'category' => $_POST['category'],
        'DDN' => $_POST['DDN'],
        'quantity' => $_POST['quantity'],
        'quantityLeft' => $_POST['quantityLeft'],
        'dateAdded' => $_POST['dateAdded'],
        'addedBy' => $_POST['addedBy'],
      );
    }


    // if(!empty($id)){
    //   $ch = curl_init($url);

    //   $opts = array(
    //     CURLOPT_RETURNTRANSFER => true,
    //     CURLOPT_POST           => TRUE,
    //     CURLOPT_POSTFIELDS     => json_encode($editBook),
    //     CURLOPT_HTTPHEADER     => array('Content-Type: application/json'),
    //     //'http' => array('method' => 'POST', 
    //     // 'header' => 'Content-Type: application/json',
    //     // 'content' => $newBook
    //   );
    
    
    //   //$fields_string = http_build_query($opts);
    //   curl_setopt_array($ch, $opts);
    
      
    //   // curl_setopt($ch, CURLOPT_URL, $url);
    //   // curl_setopt($ch, CURLOPT_POST, true);
    
    // // curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
    //   // curl_setopt($ch, CURLOPT_HEADER, 0);
    //   // curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //Set curl to return the data instead of printing it to the browser.
      
    //   $data = curl_exec($ch);
    
    //   curl_close($ch);
    
    //   // $context = stream_context_create($opts);
    //   // $returnVal = file_get_contents($url, false, $context);
    
    //   echo "<script type='text/javascript'>alert('Successfully edited');</script>";
    
    //   header("location: editBooks.php");
    // }
  ?>


  
  <div id="box"></div>
  <table id="bookTable" class="table table-striped table-bordered">
        <thead>
            <tr>
                <th>Admin Controls</th>
                <td>Title</td>
                <td>Edition</td>
                <td>ISBN</td>
                <td>Volume</td>
                <td>Total Volumes</td>
                <td>Publisher</td>
                <td>Year Published</td>
                <td>Category</td>
                <td>DDN</td>
                <td>Quantity</td>
                <td>Quantity Left</td>
                <td>Date Added</td>
                <td>Added By</td>
            </tr>
        </thead>
        <tbody>
        <!-- BOOK TABLE -->
        <?php foreach ($books as $data){
          echo "<tr>";
            echo "<input type='hidden' value='".$data->_id."'>";
            echo "<td><button  class='btn btn-edit my-2 my-sm-0'

                      id='".$data->_id."'
                      title='".$data->bookInfo->title."'
                      edition='".$data->bookInfo->edition."'
                      ISBN='".$data->bookInfo->ISBN."'
                      volume='".$data->bookInfo->volume."'
                      totalVolumes='".$data->bookInfo->totalVolumes."'
                      name='".$data->publisher->name."'
                      yearPublished='".$data->publisher->yearPublished."'
                      category='".$data->category."'
                      DDN='".$data->DDN."'
                      quantity='".$data->quantity."'
                      quantityLeft='".$data->quantityLeft."'
                      dateAdded='".$data->dateAdded."'
                      addedBy='".$data->addedBy."'

                      type='button' data-toggle='modal' data-target='#editModal'>Edit</button><button class='btn btn-delete my-2 my-sm-0' type='button' data-toggle='modal' data-target='#deleteModal'>Delete</button></td>";
            echo "<td>".$data->bookInfo->title."</td>";
            echo "<td>".$data->bookInfo->edition."</td>";
            echo "<td>".$data->bookInfo->ISBN."</td>";
            echo "<td>".$data->bookInfo->volume."</td>";
            echo "<td>".$data->bookInfo->totalVolumes."</td>";
            echo "<td>".$data->publisher->name."</td>";
            echo "<td>".$data->publisher->yearPublished."</td>";
            echo "<td>".$data->category."</td>";
            echo "<td>".$data->DDN."</td>";	
            echo "<td>".$data->quantity."</td>";	
            echo "<td>".$data->quantityLeft."</td>";	
            echo "<td>".$data->dateAdded."</td>";
            echo "<td>".$data->addedBy."</td>";
          echo "</tr>";
          } ?>
        </tbody>
  </table>

  <!-- Modal Edit -->
  <div class='modal fade' id='editModal' tabindex='-1' role='dialog' aria-labelledby='exampleModalLabel' aria-hidden='true'>
    <form name="editForm" method="POST" action="editBooks.php">
    <div class='modal-dialog' role='document'>
      <div class='modal-content'>
        <div class='modal-header'>
          <h5 class='modal-title' id='exampleModalLabel'>Update Information</h5>
          <button type='button' class='close' data-dismiss='modal' aria-label='Close'>
            <span aria-hidden='true'>&times;</span>
          </button>
        </div>
        <div class='modal-body'>
        <!-- ID FORM (CHANGE TO TYPE HIDDEN AFTER TESTING) -->
        <div id='idForm'>
          <input type='hidden' name='id' value=''>
        </div>
        <!-- ID FORM END -->
          <div id='titleForm'>
          <div class='input-group'> 
            <div class='input-group-prepend'>
              <span class='input-group-text' id='basic-addon1'>Title</span>
            </div>
            <input type='text' class='form-control' placeholder='Title' name='title' value='' aria-label='Title' aria-describedby='basic-addon2'>
            </div>
          </div>
          <div id='editionForm'>
          <div class='input-group'>
            <div class='input-group-prepend'>
              <span class='input-group-text' id='basic-addon1'>Edition</span>
            </div>
              <input type='text' class='form-control' name='edition' placeholder='Edition' value='' aria-label='Edition' aria-describedby='basic-addon2'>
            </div>
          </div>
          <div id='ISBNForm'>
          <div class='input-group'>
            <div class='input-group-prepend'>
              <span class='input-group-text' id='basic-addon1'>ISBN</span>
            </div>
              <input type='text' class='form-control' name='ISBN' placeholder='ISBN' value='' aria-label='ISBN' aria-describedby='basic-addon2'>
            </div>
          </div>
          <div id='volumeForm'>
          <div class='input-group'>
            <div class='input-group-prepend'>
              <span class='input-group-text' id='basic-addon1'>Volume</span>
            </div>
              <input type='text' class='form-control' name='volume' placeholder='Volume' value='' aria-label='Volume' aria-describedby='basic-addon2'>
            </div>
          </div>
          <div id='totalVolumesForm'>
          <div class='input-group'>
            <div class='input-group-prepend'>
              <span class='input-group-text' id='basic-addon1'>Total Volumes</span>
            </div>
              <input type='text' class='form-control' name='totalVolumes' placeholder='Total Volumes' value='' aria-label='Total Volumes' aria-describedby='basic-addon2'>
            </div>
          </div>
          <div id='publisherNameForm'>
          <div class='input-group'>
            <div class='input-group-prepend'>
              <span class='input-group-text' id='basic-addon1'>Publisher</span>
            </div>
              <input type='text' class='form-control' name='publisherName' placeholder='Publisher' value='' aria-label='Publisher' aria-describedby='basic-addon2'>
            </div>
          </div>
          <div id='yearPublishedForm'>
          <div class='input-group'>
            <div class='input-group-prepend'>
              <span class='input-group-text' id='basic-addon1'>Year Published</span>
            </div>
              <input type='text' class='form-control' name='yearPublished' placeholder='Year Published' value='' aria-label='Year Published' aria-describedby='basic-addon2'>
            </div>
          </div>
          <div id='categoryForm'>
          <div class='input-group'>
            <div class='input-group-prepend'>
              <span class='input-group-text' id='basic-addon1'>Category</span>
            </div>
              <input type='text' class='form-control' name='category' placeholder='Category' value='' aria-label='Category' aria-describedby='basic-addon2'>
            </div>
          </div>
          <div id='DDNForm'>
          <div class='input-group'>
            <div class='input-group-prepend'>
              <span class='input-group-text' id='basic-addon1'>DDN</span>
            </div>
              <input type='text' class='form-control' name='DDN' placeholder='DDN' value='' aria-label='DDN' aria-describedby='basic-addon2'>
            </div>
          </div>
          <div id='quantityForm'>
          <div class='input-group'>
            <div class='input-group-prepend'>
              <span class='input-group-text' id='basic-addon1'>Quantity</span>
            </div>
              <input type='text' class='form-control' name='quantity' placeholder='Quantity' value='' aria-label='Quantity' aria-describedby='basic-addon2'>
            </div>
          </div>
          <div id='quantityLeftForm'>
          <div class='input-group'>
            <div class='input-group-prepend'>
              <span class='input-group-text' id='basic-addon1'>Quantity Left</span>
            </div>
              <input type='text' class='form-control' name='quantityLeft' placeholder='Quantity Left' value='' aria-label='Quantity Left' aria-describedby='basic-addon2'>
            </div>
          </div>
          <div id='dateAddedForm'>
          <div class='input-group'>
            <div class='input-group-prepend'>
              <span class='input-group-text' id='basic-addon1'>Date Added (mm/dd/yyyy)</span>
            </div>
              <input type='text' class='form-control' name='dateAdded' placeholder='Date Added' value='' aria-label='Date Added' aria-describedby='basic-addon2'>
            </div>
          </div>
          <div id='addedByForm'>
          <div class='input-group'>
            <div class='input-group-prepend'>
              <span class='input-group-text' id='basic-addon1'>Added By</span>
            </div>
              <input type='text' class='form-control' name='addedBy' placeholder='Added By' value='' aria-label='Added By' aria-describedby='basic-addon2'>
            </div>
          </div>
        <div class='modal-footer'>
          <input type='submit' name='edit' value='Edit' class='btn btn-edit'>
          <button type='button' class='btn btn-secondary' data-dismiss='modal'>Close</button>
        </div>
      </div>
    </div>
    </form>
  </div>

  <!-- Modal Delete -->
  <div class='modal fade' id='deleteModal' tabindex='-1' role='dialog' aria-labelledby='exampleModalLabel' aria-hidden='true'>
    <div class='modal-dialog' role='document'>
      <div class='modal-content'>
        <div class='modal-header'>
          <h5 class='modal-title' id='exampleModalLabel'>Admin</h5>
          <button type='button' class='close' data-dismiss='modal' aria-label='Close'>
            <span aria-hidden='true'>&times;</span>
          </button>
        </div>
        <div class='modal-body'>
          Are you sure you want to delete?
        </div>
        <div class='modal-footer'>
          <button type='button' class='btn btn-delete'>Yes</button>
          <button type='button' class='btn btn-secondary' data-dismiss='modal'>No</button>
        </div>
      </div>
    </div>
  </div>
</body>
</html>

<script>
  $(document).ready(function() {
    $('#bookTable').DataTable();
  });

  $('#editModal').on('show.bs.modal', function (e) {
    // get information to update quickly to modal view as loading begins
    var opener=e.relatedTarget; //this holds the element who called the modal
    
    //GET DETAILS FROM TABLE
    var id=$(opener).attr('id');
    var title=$(opener).attr('title');
    var edition=$(opener).attr('edition');
    var ISBN=$(opener).attr('ISBN');
    var volume=$(opener).attr('volume');
    var totalVolumes=$(opener).attr('totalVolumes');
    var publisherName=$(opener).attr('name');
    var yearPublished=$(opener).attr('yearPublished');
    var category=$(opener).attr('category');
    var DDN=$(opener).attr('DDN');
    var quantity=$(opener).attr('quantity');
    var quantityLeft=$(opener).attr('quantityLeft');
    var dateAdded=$(opener).attr('dateAdded');
    var addedBy=$(opener).attr('addedBy');


    //SET TO THE FORM FOR THAT VARIABLE, AND SETS THE VALUE OF THE INPUT
    $('#idForm').find('[name="id"]').val(id);
    $('#titleForm').find('[name="title"]').val(title);
    $('#editionForm').find('[name="edition"]').val(edition);
    $('#ISBNForm').find('[name="ISBN"]').val(ISBN);
    $('#volumeForm').find('[name="volume"]').val(volume);
    $('#totalVolumesForm').find('[name="totalVolumes"]').val(totalVolumes);
    $('#publisherNameForm').find('[name="publisherName"]').val(publisherName);
    $('#yearPublishedForm').find('[name="yearPublished"]').val(yearPublished);
    $('#categoryForm').find('[name="category"]').val(category);
    $('#DDNForm').find('[name="DDN"]').val(DDN);
    $('#quantityForm').find('[name="quantity"]').val(quantity);
    $('#quantityLeftForm').find('[name="quantityLeft"]').val(quantityLeft);
    $('#dateAddedForm').find('[name="dateAdded"]').val(dateAdded);
    $('#addedByForm').find('[name="addedBy"]').val(addedBy);
  });
</script>