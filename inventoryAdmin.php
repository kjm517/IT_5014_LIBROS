<html>
<head>
    <link rel="stylesheet" href="css/datatables.min.css">
</head>
<body>

  <?php 
  include('links.php');
    include 'navbar_Admin.php';
    $apiKey = 'Z5EGkpadDXuof73_eCtBo5r68HygNdhj';
    $url = 'https://api.mlab.com/api/1/databases/libros/collections/book?apiKey=Z5EGkpadDXuof73_eCtBo5r68HygNdhj';
    $json = file_get_contents($url);
    $books = json_decode($json);

    if(!empty($_POST)){
      $id = $_POST['id'];
      $editBook = array (
        'title' => $_POST['title'],
        'ISBN' => $_POST['ISBN'],
        'volume' => $_POST['volume'],
        'totalVolumes' => $_POST['totalVolumes'],
        'publisherName' => $_POST['publisherName'],
        'yearPublished' => $_POST['yearPublished'],
        'category' => $_POST['category'],
        'DDN' => $_POST['DDN'],
        'quantity' => $_POST['quantity'],
        'quantityLeft' => $_POST['quantityLeft'],
        'dateAdded' => $_POST['dateAdded'],
        'addedBy' => $_POST['addedBy'],
      );
    }
  ?>


  
  <div id="box">
  	<button id='add' type="button" class="btn btn-default" onclick="window.location.href='addBooks.php'">ADD BOOK</button>
  </div>
  
  <table id="bookTable" class="table table-striped table-bordered">
        <thead>
            <tr>
                <th>Admin Controls</th>
                <td>Title</td>
                <td>Edition</td>
                <td>Volume</td>
                <td>Publisher</td>
                <td>Year Published</td>
                <td>Category</td>
                <td>Quantity Left</td>
                <td>Date Added</td>
                <td>Added By</td>
                <td>View Books</td>
            </tr>
        </thead>
        <tbody>
        <!-- BOOK TABLE -->
        <?php foreach ($books as $data){
          echo "<tr>";
            echo "<input type='hidden' value='".$data->_id."'>";
            echo "<td><button  class='btn btn-edit my-2 my-sm-0'

                      id='".$data->_id."'
                      title='".$data->bookInfo->title."'
                      edition='".$data->bookInfo->edition."'
                      ISBN='".$data->bookInfo->ISBN."'
                      volume='".$data->bookInfo->volume."'
                      totalVolumes='".$data->bookInfo->totalVolumes."'
                      name='".$data->publisher->name."'
                      yearPublished='".$data->publisher->yearPublished."'
                      category='".$data->category."'
                      DDN='".$data->DDN."'
                      quantity='".$data->quantity."'
                      quantityLeft='".$data->quantityLeft."'
                      dateAdded='".$data->dateAdded."'
                      addedBy='".$data->addedBy."'

                      type='button' data-toggle='modal' data-target='#editModal'>Edit</button><button class='btn btn-delete my-2 my-sm-0' type='button' onClick=\"return confirm('Are you sure you want to delete?')\">Delete</button></td>";
            echo "<td>".$data->bookInfo->title."</td>";
            echo "<td>".$data->bookInfo->edition."</td>";
            echo "<td>".$data->bookInfo->volume."</td>";
            echo "<td>".$data->publisher->name."</td>";
            echo "<td>".$data->publisher->yearPublished."</td>";
            echo "<td>".$data->category."</td>";
            echo "<td>".$data->quantityLeft."</td>";	
            echo "<td>".$data->dateAdded."</td>";
            echo "<td>".$data->addedBy."</td>";
            echo "<td><button class='btn btn-edit my-2 my-sm-0'

                      id='".$data->_id."'
                      title='".$data->bookInfo->title."'
                      edition='".$data->bookInfo->edition."'
                      ISBN='".$data->bookInfo->ISBN."'
                      volume='".$data->bookInfo->volume."'
                      totalVolumes='".$data->bookInfo->totalVolumes."'
                      name='".$data->publisher->name."'
                      yearPublished='".$data->publisher->yearPublished."'
                      category='".$data->category."'
                      DDN='".$data->DDN."'
                      quantity='".$data->quantity."'
                      quantityLeft='".$data->quantityLeft."'
                      dateAdded='".$data->dateAdded."'
                      addedBy='".$data->addedBy."'

                      type='button' data-toggle='modal' data-target='#viewBookModal'>View</button></td>";
          echo "</tr>";
        
          } ?>
        </tbody>
  </table>

  <!-- Modal Edit -->
  <div class='modal fade' id='editModal' tabindex='-1' role='dialog' aria-labelledby='exampleModalLabel' aria-hidden='true'>
    <form name="editForm" method="POST" action="inventoryAdmin.php">
    <div class='modal-dialog' role='document'>
      <div class='modal-content'>
        <div class='modal-header'>
          <h5 class='modal-title' id='exampleModalLabel'>Update Information</h5>
          <button type='button' class='close' data-dismiss='modal' aria-label='Close'>
            <span aria-hidden='true'>&times;</span>
          </button>
        </div>
        <div class='modal-body'>
        <!-- ID FORM (CHANGE TO TYPE HIDDEN AFTER TESTING) -->
        <div id='idForm'>
          <input type='hidden' name='id' value=''>
        </div>
        <!-- ID FORM END -->
          <div id='titleForm'>
          <div class='input-group'> 
            <div class='input-group-prepend'>
              <span class='input-group-text' id='basic-addon1'>Title</span>
            </div>
            <input type='text' class='form-control' placeholder='Title' name='title' value='' aria-label='Title' aria-describedby='basic-addon2'>
            </div>
          </div>
          <div id='editionForm'>
          <div class='input-group'>
            <div class='input-group-prepend'>
              <span class='input-group-text' id='basic-addon1'>Edition</span>
            </div>
              <input type='text' class='form-control' name='edition' placeholder='Edition' value='' aria-label='Edition' aria-describedby='basic-addon2'>
            </div>
          </div>
          <div id='ISBNForm'>
          <div class='input-group'>
            <div class='input-group-prepend'>
              <span class='input-group-text' id='basic-addon1'>ISBN</span>
            </div>
              <input type='text' class='form-control' name='ISBN' placeholder='ISBN' value='' aria-label='ISBN' aria-describedby='basic-addon2'>
            </div>
          </div>
          <div id='volumeForm'>
          <div class='input-group'>
            <div class='input-group-prepend'>
              <span class='input-group-text' id='basic-addon1'>Volume</span>
            </div>
              <input type='text' class='form-control' name='volume' placeholder='Volume' value='' aria-label='Volume' aria-describedby='basic-addon2'>
            </div>
          </div>
          <div id='totalVolumesForm'>
          <div class='input-group'>
            <div class='input-group-prepend'>
              <span class='input-group-text' id='basic-addon1'>Total Volumes</span>
            </div>
              <input type='text' class='form-control' name='totalVolumes' placeholder='Total Volumes' value='' aria-label='Total Volumes' aria-describedby='basic-addon2'>
            </div>
          </div>
          <div id='publisherNameForm'>
          <div class='input-group'>
            <div class='input-group-prepend'>
              <span class='input-group-text' id='basic-addon1'>Publisher</span>
            </div>
              <input type='text' class='form-control' name='publisherName' placeholder='Publisher' value='' aria-label='Publisher' aria-describedby='basic-addon2'>
            </div>
          </div>
          <div id='yearPublishedForm'>
          <div class='input-group'>
            <div class='input-group-prepend'>
              <span class='input-group-text' id='basic-addon1'>Year Published</span>
            </div>
              <input type='text' class='form-control' name='yearPublished' placeholder='Year Published' value='' aria-label='Year Published' aria-describedby='basic-addon2'>
            </div>
          </div>
          <div id='categoryForm'>
          <div class='input-group'>
            <div class='input-group-prepend'>
              <span class='input-group-text' id='basic-addon1'>Category</span>
            </div>
              <input type='text' class='form-control' name='category' placeholder='Category' value='' aria-label='Category' aria-describedby='basic-addon2'>
            </div>
          </div>
          <div id='DDNForm'>
          <div class='input-group'>
            <div class='input-group-prepend'>
              <span class='input-group-text' id='basic-addon1'>DDN</span>
            </div>
              <input type='text' class='form-control' name='DDN' placeholder='DDN' value='' aria-label='DDN' aria-describedby='basic-addon2'>
            </div>
          </div>
          <div id='quantityForm'>
          <div class='input-group'>
            <div class='input-group-prepend'>
              <span class='input-group-text' id='basic-addon1'>Quantity</span>
            </div>
              <input type='text' class='form-control' name='quantity' placeholder='Quantity' value='' aria-label='Quantity' aria-describedby='basic-addon2'>
            </div>
          </div>
          <div id='quantityLeftForm'>
          <div class='input-group'>
            <div class='input-group-prepend'>
              <span class='input-group-text' id='basic-addon1'>Quantity Left</span>
            </div>
              <input type='text' class='form-control' name='quantityLeft' placeholder='Quantity Left' value='' aria-label='Quantity Left' aria-describedby='basic-addon2'>
            </div>
          </div>
          <div id='dateAddedForm'>
          <div class='input-group'>
            <div class='input-group-prepend'>
              <span class='input-group-text' id='basic-addon1'>Date Added (mm/dd/yyyy)</span>
            </div>
              <input type='text' class='form-control' name='dateAdded' placeholder='Date Added' value='' aria-label='Date Added' aria-describedby='basic-addon2'>
            </div>
          </div>
          <div id='addedByForm'>
          <div class='input-group'>
            <div class='input-group-prepend'>
              <span class='input-group-text' id='basic-addon1'>Added By</span>
            </div>
              <input type='text' class='form-control' name='addedBy' placeholder='Added By' value='' aria-label='Added By' aria-describedby='basic-addon2'>
            </div>
          </div>
        <div class='modal-footer'>
          <input type='submit' name='edit' value='Edit' class='btn btn-edit'>
          <button type='button' class='btn btn-secondary' data-dismiss='modal'>Close</button>
        </div>
      </div>
    </div>
        </div>
    </form>
  </div>
      

  <!-- Modal Delete -->
  <div class='modal fade' id='deleteModal' tabindex='-1' role='dialog' aria-labelledby='exampleModalLabel' aria-hidden='true'>
    <div class='modal-dialog' role='document'>
      <div class='modal-content'>
        <div class='modal-header'>
          <h5 class='modal-title' id='exampleModalLabel'>Admin</h5>
          <button type='button' class='close' data-dismiss='modal' aria-label='Close'>
            <span aria-hidden='true'>&times;</span>
          </button>
        </div>
        <div class='modal-body'>
          Are you sure you want to delete?
        </div>
        <div class='modal-footer'>
          <button type='button' class='btn btn-delete'>Yes</button>
          <button type='button' class='btn btn-secondary' data-dismiss='modal'>No</button>
        </div>
      </div>
    </div>
  </div>
</body>
</html>

 <!-- MODAL VIEW BOOK -->
		<div class="modal fade" id="viewBookModal" tabindex="-1" role="dialog" aria-labelledby="viewBookModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="viewBookModalLabel">View Book</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						</div>
						<div class="modal-body">
						<center>
                            <div id='idForm'>
                                    <input type='hidden' name='id' value=''>
                            </div>
                            <h3 id="viewTitle"></h3>
							<p><i><span id="viewEdition"></span>th edition</i></p>
                            <p>Volume <span id='viewVolume'></span> (out of <span id="viewTotalVolumes"> </span> volumes.)</p>
                            <p><b>Publisher: </b><span id='viewPublisher'></span> (<span id='viewYearPublished'></span>)</p>
                        </center>
                            <p><b>Category: </b><span id='viewCategory'></span></p>
							<p><b>ISBN: </b><span id='viewISBN'></span></p>
                            <p><b>DDN: </b><span id='viewDDN'></span></p>
                            <p><b>Quantity: </b><span id="viewQuantityLeft"></span> out of <span id="viewQuantity"></span></p>
							
					</div> 
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
		<!-- END OF MODAL VIEW BOOK -->

<script>
  $(document).ready(function() {
    $('#bookTable').DataTable();
  });

  $('#editModal').on('show.bs.modal', function (e) {
    // get information to update quickly to modal view as loading begins
    var opener=e.relatedTarget; //this holds the element who called the modal
    
    //GET DETAILS FROM TABLE
    var id=$(opener).attr('id');
    var title=$(opener).attr('title');
    var edition=$(opener).attr('edition');
    var ISBN=$(opener).attr('ISBN');
    var volume=$(opener).attr('volume');
    var totalVolumes=$(opener).attr('totalVolumes');
    var publisherName=$(opener).attr('name');
    var yearPublished=$(opener).attr('yearPublished');
    var category=$(opener).attr('category');
    var DDN=$(opener).attr('DDN');
    var quantity=$(opener).attr('quantity');
    var quantityLeft=$(opener).attr('quantityLeft');
    var dateAdded=$(opener).attr('dateAdded');
    var addedBy=$(opener).attr('addedBy');


    //SET TO THE FORM FOR THAT VARIABLE, AND SETS THE VALUE OF THE INPUT
    $('#idForm').find('[name="id"]').val(id);
    $('#titleForm').find('[name="title"]').val(title);
    $('#editionForm').find('[name="edition"]').val(edition);
    $('#ISBNForm').find('[name="ISBN"]').val(ISBN);
    $('#volumeForm').find('[name="volume"]').val(volume);
    $('#totalVolumesForm').find('[name="totalVolumes"]').val(totalVolumes);
    $('#publisherNameForm').find('[name="publisherName"]').val(publisherName);
    $('#yearPublishedForm').find('[name="yearPublished"]').val(yearPublished);
    $('#categoryForm').find('[name="category"]').val(category);
    $('#DDNForm').find('[name="DDN"]').val(DDN);
    $('#quantityForm').find('[name="quantity"]').val(quantity);
    $('#quantityLeftForm').find('[name="quantityLeft"]').val(quantityLeft);
    $('#dateAddedForm').find('[name="dateAdded"]').val(dateAdded);
    $('#addedByForm').find('[name="addedBy"]').val(addedBy);
      
    
      
  });
$('#viewBookModal').on('show.bs.modal', function (e) {
    // get information to update quickly to modal view as loading begins
    var opener=e.relatedTarget; //this holds the element who called the modal
    
    //GET DETAILS FROM TABLE
    var id=$(opener).attr('id');
    var title=$(opener).attr('title');
    var edition=$(opener).attr('edition');
    var ISBN=$(opener).attr('ISBN');
    var volume=$(opener).attr('volume');
    var totalVolumes=$(opener).attr('totalVolumes');
    var publisherName=$(opener).attr('name');
    var yearPublished=$(opener).attr('yearPublished');
    var category=$(opener).attr('category');
    var DDN=$(opener).attr('DDN');
    var quantity=$(opener).attr('quantity');
    var quantityLeft=$(opener).attr('quantityLeft');
    var dateAdded=$(opener).attr('dateAdded');
    var addedBy=$(opener).attr('addedBy');
    
    //FOR VIEW BOOKS
     $('#idForm').find('[name="id"]').val(id);
    document.getElementById("viewTitle").innerHTML= title;
    document.getElementById("viewEdition").innerHTML= edition;
    document.getElementById("viewISBN").innerHTML= ISBN;  
    document.getElementById("viewVolume").innerHTML= volume;
    document.getElementById("viewTotalVolumes").innerHTML= totalVolumes;
    document.getElementById("viewPublisher").innerHTML= publisherName; 
    document.getElementById("viewYearPublished").innerHTML= yearPublished; 
    document.getElementById("viewCategory").innerHTML= category; 
    document.getElementById("viewDDN").innerHTML= DDN; 
    document.getElementById("viewQuantity").innerHTML= quantity; 
    document.getElementById("viewQuantityLeft").innerHTML= quantityLeft;
    document.getElementById("viewDateAdded").innerHTML= dateAdded; 
    document.getElementById("viewAddedBy").innerHTML= addedBy;
    
 });
</script>