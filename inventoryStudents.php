<head>
    <?php //include("links.php"); ?>
</head>
<style>

</style>
<body>
    <?php include 'navbar_Student.php' ?>
	<div id="box">
		<table id="bookTable" class="table table-striped table-bordered">
			<thead>
	            <tr>
	                <th>Book Cover</th>
	                <th>Title</th>
	                <th>Stock</th>
					<th>View Books</th>
	            </tr>
	        </thead>
	        <tbody>
	            <tr>
	            	<td><img class="bookSize" src="img/pride.jpg"></td>
	                <td>Pride and Prejudice</td>
	                <td>61</td>
					<td><a href="#" class="btn btn-warning viewBookBtn" role="button" data-toggle="modal" data-target="#viewBookModal">View Book</a></td>
	            </tr>
	        </tbody>
        
		
			<!-- MODAL VIEW BOOK -->
		<div class="modal fade" id="viewBookModal" tabindex="-1" role="dialog" aria-labelledby="viewBookModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="viewBookModalLabel">View Book</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						</div>
						<div class="modal-body">
						<center>
							<h3>Pride and Prejudice</h3>
							<p><b>Jane Austen</b></p>
							<p>Rex Publishing Inc.</p>
							<p>2007</p>
							<p>4091219312</p>
							<br>
							<img src="img/pride.jpg" alt="Sample Image">
						</center>
						<br>
						<p>While the arrival of wealthy gentlemen sends her marriage-minded mother into a frenzy, willful and opinionated Elizabeth Bennet matches wits with haughty Mr. Darcy.</p>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
	    </table>
	</div>
	
	<!-- MODAL VIEW BOOK -->
		<div class="modal fade" id="viewBookModal" tabindex="-1" role="dialog" aria-labelledby="viewBookModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="viewBookModalLabel">View Book</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						</div>
						<div class="modal-body">
						<center>
							<h3>Pride and Prejudice</h3>
							<p><b>Jane Austen</b></p>
							<p>Rex Publishing Inc.</p>
							<p>2007</p>
							<p>4091219312</p>
							<br>
							<img src="img/pride.jpg" alt="Sample Image">
						</center>
						<br>
						<p>While the arrival of wealthy gentlemen sends her marriage-minded mother into a frenzy, willful and opinionated Elizabeth Bennet matches wits with haughty Mr. Darcy.</p>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
		<!-- END OF MODAL VIEW BOOK -->
		
</body>
</html>

<script>
  $(document).ready(function() {
    $('#bookTable').DataTable();
  });

</script>
