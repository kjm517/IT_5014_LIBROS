<?php 
session_start();

$studentID = '15100001';

if($_SESSION['books'] == NULL){
  $reserveBook = array('eb1a3227cdc3fedbaec2fe38bf6c044a','5b3c4af4fb6fc03328f99862','5b3c4af4fb6fc03328f99863');
  $_SESSION['books'] = $reserveBook;
}

// $reserveBook = array('eb1a3227cdc3fedbaec2fe38bf6c044a','5b3c4af4fb6fc03328f99862','5b3c4af4fb6fc03328f99863');
// $_SESSION['books'] = $reserveBook;

$apiKey = 'V1yTg1WeLN5ameZzsTGx4SS0n_pMOWj0';
$booksUrl = 'https://api.mlab.com/api/1/databases/libros/collections/book?apiKey=' .$apiKey;

$booksJson = file_get_contents($booksUrl);
$books = json_decode($booksJson);

$date = date("m/d/Y");
$time = date("H:i:s"); 

?>

<html>
  <head>
        <?php include("navbar_Student.php"); ?>
        <?php include("links.php"); ?>
    </head>
    <style>
        <?php include("css/Cart.css"); ?>
    </style>
  
<body>

<!--**************************************** CART START *****************************************-->
  <!-- Product -->
    <div class="container">   
      <div class="row">
        <div class="col-md-12">
          <form>
    
            <h2 class="h2bold">MY BOOKS</h2>
            <p class="text-muted">You currently have <?php echo count($_SESSION['books'])?> book(s) in your shelf.</p>
            <!--********************** TABLE **********************-->
              <table class="table table-hover">
                <thead class = "thead-green">
                  <tr>
                    <!-- IMAGE -->
                    <th>Book</th>
                    <!-- TITLE -->
                    <th></th>
                    <!-- AUTHOR -->
                    <th>Author</th>
                    <!-- AVAILABILITY -->
                    <th>Availability</th>
                    <!-- REMOVE -->
                    <th></th>
                    <th></th>
                  </tr>
                </thead>
                <tbody class="tbody-green">
                  <?php 

                  foreach($books as $book){

                    $bookID_DB = $book->_id;
    
                    // foreach ($bookID_DB as $key => $value) {
                    
                      foreach ($_SESSION['books'] as $keys=>$values) {
                  
                        if($bookID_DB == $values){
                          $bookTitle = $book->bookInfo->title;
                          $authors = $book->contributors->authors;
                          foreach ($authors as $keyAuthor => $valueAuthor) {
                            $authorName = $valueAuthor->firstName.' '.$valueAuthor->middleInitial.'. '.$valueAuthor->lastName;
                          
                          }
                        ?>

                      <tr>
                    <!-- IMAGE -->
                    <form action="ReserveBook.php" method="POST">
                      <td style="width:85px">
                        <a href="#">
                          <img src="img/physics.jpg">
                        </a>
                      </td>
                      <!-- TITLE -->
                      <td style="width:274px">
                        <a href="inventoryStudents.php" class="">
                          <b><?php echo $bookTitle?></b>
                          <input type='hidden' name='sid' value='<?php echo $studentID ?>'>
                          <input type='hidden' name='bid' value='<?php echo $values?>'>
                          <input type='hidden' name='cdate' value='<?php echo $date ?>'>
                          <input type='hidden' name='ctime' value='<?php echo $time ?>'>
                        </a>
                      </td>
                      <!-- AUTHOR -->
                      <td><?php echo $authorName?></td>
                      <!-- AVAILABILITY -->
                      <td>Available</td>
                      <!--INSERT-->
                      <td style="text-align:center">
                        <button type="submit" class="btn btn-outline-success">
                          <i class="fa fa-book fa-book-color"></i>
                        </button>
                      </td>
                    </form>

                    <!-- REMOVE -->
                    <form action="CartDelete.php" method="POST">
                    <td style="text-align:center"><button class="btn btn-outline-danger" value="<?php $book->_id ?>"><i class="fa fa-trash-o"></button></i></a></td>
                    </tr>
                  </form>
                  
                


                      <?php

                      }
                    }
                  }


                  ?>
                  
                   
                </tbody>
                <tfoot>
                  <tr>
                    <th colspan="12"></th>
                  </tr>
                </tfoot>
              </table>
            <!--********************* END OF TABLE ***********************-->
          </form>
      </div>
    </div>
  </div>
  <!--**************************************** END OF CART *****************************************-->

</body>
</html>