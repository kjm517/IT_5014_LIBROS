<html>
<head>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/datatables.min.css">
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
  <table id="bookTable" class="table table-striped table-bordered">
        <thead>
            <tr>
                <th>Admin Controls</th>
                <th>Title</th>
                <th>ISBN</th>
                <th>Description</th>
                <th>Stock</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><button class="btn btn-edit my-2 my-sm-0" type="button" data-toggle="modal" data-target="#editModal">Edit</button> <button class="btn btn-delete my-2 my-sm-0" type="button" data-toggle="modal" data-target="#deleteModal">Delete</button></td>
                <td>Catcher in the Rye</td>
                <td>4091219312</td>
                <td>He catches Rye a lot.</td>
                <td>61</td>
            </tr>
        </tbody>
  </table>
  <!-- Button trigger modal -->
  <!--
  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
    Launch demo modal
  </button>
  -->

  <!-- Modal Edit -->
  <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Login</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="input-group mb-3"> 
            <div class="input-group-prepend">
              <span class="input-group-text" id="basic-addon1">Title</span>
            </div>
            <input type="text" class="form-control" placeholder="Title" value="Catcher in the Rye" aria-label="Title" aria-describedby="basic-addon2">
        </div>
          <div class="input-group mb-3">
            <div class="input-group-prepend">
              <span class="input-group-text" id="basic-addon1">ISBN</span>
            </div>
            <input type="text" class="form-control" aria-label="ISBN" placeholder="ISBN" value="4091219312" aria-label="ISBN" aria-describedby="basic-addon2">
          </div>
          <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text">Description</span>
            </div>
            <input type="text" class="form-control" aria-label="Description" placeholder="Description" value="He catches Rye a lot." aria-label="Description" aria-describedby="basic-addon2">
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-edit">Edit</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>

  <!-- Modal Delete -->
  <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Admin</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          Are you sure you want to delete?
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-delete">Yes</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
        </div>
      </div>
    </div>
  </div>
</body>

<script src="js/jquery.min.js"></script>
<script src="js/jquery.datatables.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/datatables.min.js"></script>
<script src="js/dataTables.bootstrap4.min.js"></script>
<script>
  $(document).ready(function() {
    $('#bookTable').DataTable();
  });
</script>
</html>