<?php
	if(isset($_POST["idNum"]) && isset($_POST['pass'])){
	    validate($_POST["idNum"], $_POST['pass']);
	}else{
	    // check which one is lacking
	    if(!isset($_POST["idNum"]) && !isset($_POST["pass"])){
	    	$_SESSION["error_msg"] = "Please input your username and password!";
	    }else if(!isset($_POST["idNum"])){
	    	$_SESSION["error_msg"] = "Please input your username!";
	    }else{
	    	$_SESSION["error_msg"] = "Please input your password!";
	    }
	    // redirect balik to the login page
	    header("location: login.php");
	}

	function validate($idNum, $pass){
	    global $user;
	    
	    include('config.php');
	    $staff = $user->findOne(
	        array('_id' => $idNum, 'password' => $pass, 'staffType' => 'staff')
	    );
	    
		$student = $user->findOne(
	        array('_id' => $idNum, 'password' => $pass, 'staffType' => 'N/A')
	    );

	    if($staff){
	    	$_SESSION["logged_in"] = $idNum;
    		header('location: inventoryAdmin.php');	
    	} else if($student){
    		$_SESSION["logged_in"] = $idNum;
    		header('location: inventoryStudents.php');	
    	} else {
    		$_SESSION["error_msg"] = "Error! Invalid username or password!";
	    	header('location: login.php');
	    }
	}
?>