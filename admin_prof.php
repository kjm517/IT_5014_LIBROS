<?php
    include('config.php');
    //session_start();

    //$adminID = $_GET['adminID'];
    $adminID =  "15100002";

    $type = "student_type";
    $lName = "lastname";
    $fName = "firstname";
    $mInitial = "middleInitial";
    $course = "course";
    $dept = "department";
    $yearLevel = "yearLevel";
    $borrowStatus = "cleared / blocked";
    $borrowValidation = "validated";

    foreach ($users as $admin) {
        if ($admin->_id == $adminID) {
            $type = $admin->student_type;
            $lName = $admin->name->lastname;
            $fName = $admin->name->firstname;
            $mInitial = $admin->name->middleInitial;
            $course = $admin->academicInfo->course;
            $dept = $admin->academicInfo->department;
            $yearLevel = $admin->academicInfo->yearLevel;
            $borrowStatus = $admin->borrowStatus;
            $borrowValidation = $admin->borrowValidation;
        }
    }
?>
<link rel="stylesheet" href="css/profile.css">

<?php include("navbar_Admin.php"); ?>
    
    <div class="centering">
      <div class="row">
        <div class="col-md-3">
          <div class="card text-center">
            <img class="card-img-top" src="img/avatar.jpg" alt="Profile Picture">
            <div class="card-body">
                <h4 class="card-title">
                    <?php echo $fName." ".$mInitial.". ".$lName; ?>
                </h4>
                <h5 class="card-text">
                    <?php echo $adminID; ?>
                </h5>
                <p class="title">
                    <?php echo $type; ?>
                    <br>
                    <?php echo $course."-".$yearLevel; ?>
                </p>

                <?php
                    if ($type == "working-student" && $borrowStatus == "cleared") {
                        echo '<div class="alert alert-success" role="alert">Cleared</div>';
                    }else{
                        echo '<div class="alert alert-danger" role="alert">Blocked</div>';
                    }
                ?>
                <div class="dropdown">
                  <button class="btn btn-next dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">View History
                  </button>
                  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <a class="dropdown-item" href="#borrowed" data-toggle="modal">Borrowed Books</a>
                    <a class="dropdown-item" href="#reserved" data-toggle="modal">Reserved Books</a>
                    <a class="dropdown-item" href="#past" data-toggle="modal">Past Dues</a>
                  </div>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- BORROWED BOOKS Modal -->
    <div class="modal fade" id="borrowed" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Borrowed Books</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <table class="table table-striped table-font">
              <thead>
                  <th>Issue Date</th>
                  <th>Return Date</th>
                  <th>Book</th>
                  <th>Student Name</th>
                  <th>Program and Year</th>
              </thead>
              <tr>
                  <td>June 5, 2018</td>
                  <td>June 8, 2018</td>
                  <td>Data Structures</td>
                  <td>Kris Capao</td>
                  <td>BSIT-4</td>
              </tr>
          </table>
          </div>
        </div>
      </div>

    <!-- <button class="btn btn-previous"><img src="package/build/svg/arrow-left.svg"> Previous</button>
    <button class="btn btn-next">Next <img src="package/build/svg/arrow-right.svg"></button> -->
    
    </div>

    <!-- RESERVED BOOKS Modal -->
    <div class="modal fade" id="reserved" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Reserved Books</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <table class="table table-striped text-center table-font">
              <thead>
                  <th>Issue Date</th>
                  <th>Book</th>
                  <th>Student Name</th>
                  <th>Program and Year</th>
                  <th>Validity</th>
              </thead>
              <tr>
                  <td>June 5, 2018</td>
                  <td>Data Structures</td>
                  <td>Kris Capao</td>
                  <td>BSIT-4</td>
                  <td>Validated</td>
              </tr>
          </table>
          </div>
        </div>
      </div>

    <!-- <button class="btn btn-previous"><img src="package/build/svg/arrow-left.svg"> Previous</button>
    <button class="btn btn-next">Next <img src="package/build/svg/arrow-right.svg"></button> -->
    
    </div>

    <!-- PAST DUES Modal -->
    <div class="modal fade" id="past" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Past Dues</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <table class="table table-striped text-center table-font">
              <thead>
                <th>Fine Amount</th>
                <th>Student Name</th>
                <th>Department</th>
                <th>Program and Year</th>
                <th>Reason</th>
              </thead>
              <tr>
                <td>Php 5</td>
                <td><?php echo $fName." ".$mInitial.". ".$lName; ?></td>
                <td><?php echo $dept; ?></td>
                <td><?php echo $course."-".$yearLevel ?></td>
                <td>Had to finish thesis</td>
              </tr>
          </table>
          </div>
        </div>
      </div>

    <!-- <button class="btn btn-previous"><img src="package/build/svg/arrow-left.svg"> Previous</button>
    <button class="btn btn-next">Next <img src="package/build/svg/arrow-right.svg"></button> -->
    
    </div>
    <!---Right side End--->

<!---Profile Section End--->

</body>

</html>

<script>
</script>
