<?php
	include 'navbar_Student.php';
	//session_start();

	//$studentID = $_GET['studentID'];
	$studentID = '15100001';

	$apiKey = 'V1yTg1WeLN5ameZzsTGx4SS0n_pMOWj0';

	$url = 'https://api.mlab.com/api/1/databases/libros/collections/user?apiKey=' .$apiKey;
	$booksUrl = 'https://api.mlab.com/api/1/databases/libros/collections/book?apiKey=' .$apiKey;
	$reservationUrl = 'https://api.mlab.com/api/1/databases/libros/collections/reservation?apiKey=' .$apiKey;

	//Get the student's info
	$json = file_get_contents($url);
	$users = json_decode($json);
	// book info
	$booksJson = file_get_contents($booksUrl);
	$books = json_decode($booksJson);
	// reservation info
	$reservationJson = file_get_contents($reservationUrl);
	$reservations = json_decode($reservationJson);

	$studentLName = "last name";
	$studentFName = "first name";
	$studentMInitial = "middle initial";
	$course = "course";
	$dept = "department";
	$yearLevel = "year level";
	$borrowStatus = "cleared / blocked";

	foreach ($users as $student) {
		if ($student->_id == $studentID) {
			$studentLName = $student->name->lastname;
			$studentFName = $student->name->firstname;
			$studentMInitial = $student->name->middleInitial;
			$course = $student->academicInfo->course;
			$dept = $student->academicInfo->department;
			$yearLevel = $student->academicInfo->yearLevel;
			$borrowStatus = $student->borrowStatus;
		}
	}

	//GET CURRENTLY BORROWED books
	$bookTitle = "Book Title";
	$bookEdition = "edition";
	$bookISBN = "ISBN";
	$bookVolume = "volume";
	$totalVolume = " ";
	$bookTVolumes = "totalVolumes";
	$bookID = "5b3c4af4fb6fc03328f99863";

	//contributors
	$authorFname = "first name";
	$authorMI = "middle initial";
	$authorLname = "last name";
	$authorMI = "middle initial";
	$description = "description";
	$bookImage = "Book Image";

	//PUBLISHER
	$pubName = "";
	$street = "";
	$city = "";
	$state = "";
	$country = "";
	$zip = "";
	$year_pub = "";
	$category = "";

foreach($books as $book){
	if($book->_id == $bookID){
		$bookTitle = $book->bookInfo->title;
		$description = $book->bookInfo->description;
		$bookImage = $book->bookInfo->image;

		$authorFname = $book->contributors->authors[0]->firstName;
		$authorLName = $book->contributors->authors[0]->lastName;
		$authorMI = $book->contributors->authors[0]->middleInitial;

		$pubName = $book->publisher->name;
		$street = $book->publisher->address->street;
		$city = $book->publisher->address->city;
		$state = $book->publisher->address->state;
		$country = $book->publisher->address->country;
		$zip = $book->publisher->address->zip;
		$year_pub = $book->publisher->yearPublished;
		$category = $book->category;
	}
}


?>
<html>
	<!-- START OF HTML BODY -->
	<body overflow-y: "hidden">

		<div class="container">
			<div class="row">
				<div clas="col-md-8 col-md-offset-2" id="centerRow">
					<div class="row">
						<div class="col col-md-4" id="centerTitle">
							<img src="img/banner.png">
						</div>
					</div>

					<!-- STUDENT DETAILS -->
					<div class="row">
						<div class="col col-md-3">
							<img src="img/avatar.jpg" id="studentImage" name="studImage">
						</div>
						<div class="col col-md-6">
							<p class="identifier">student id</p>
								<?php
									echo "<p class='studentDetails' name='studentID'>" .$studentID. "</p>"
								?>
							<p class="identifier">name</p>
								<?php
									echo "<p class='studentDetails' name='studentName'>".$studentLName.", ".$studentFName." ".$studentMInitial.".</p>";
								?>
							<p class="identifier">program and year</p>
								<?php
									echo "<p class='studentDetails' name='studentProgram'>".$course." - ".$yearLevel."</p>";
								?>
							<p class="identifier"> department </p>
								<?php
									echo "<p class='studentDetails' name='studentDept'>".$dept."</p>"
								?>
						</div>
						<div class="col col-md-3">
							<p class="identifier">student status</p>
							<div class="studentStatusDiv">
								<?php
									if ($borrowStatus == 'cleared') {
										echo "<button class='btn btn-default' style='background-color:#00C853;color:white;font-weight:bold'>
											  	<span class='glyphicon glyphicon-ok-sign'></span> Cleared
											  </button>
											  <button disabled class='btn btn-default' style='background-color:#f25648;color:white;'>
											  	<span class='glyphicon glyphicon-remove-sign'></span> Fined
											  </button>";
									}else{
										echo "<button disabled class='btn btn-default' style='background-color:#00C853;color:white;font-weight:bold'>
											  	<span class='glyphicon glyphicon-ok-sign'></span> Cleared
											  </button>
											  <button class='btn btn-default' style='background-color:#f25648;color:white;'>
											  	<span class='glyphicon glyphicon-remove-sign'></span> Fined
											  </button>";
									}
								?>

							</div>
						</div>
					</div>
					<!-- END OF STUDENT DETAILS -->
					<!-- TABBED PAGES -->
					<div id="studentTabs">
						<div class="studentTab">
						  <button class="tablinks" onclick="openTab(event, 'Currently_Borrowed')">currently borrowed books</button>
						  <button class="tablinks" onclick="openTab(event, 'Reserved_Books')">reserved books</button>
						  <button class="tablinks" onclick="openTab(event, 'History')">borrowing history</button>
						</div>

						<div id="Currently_Borrowed" class="tabcontent">
						  <h3 class="tabSentences">You are currently borrowing these books:</h3>
						  <div class="row">
							  <div class="col-md-4">
							    <div class="thumbnail">
							      <img id="cover" src="<?php echo $bookImage?>" alt="Sample Image">
							      <div class="caption">
											<?php
												echo "<h1 name='bTitle'>".$bookTitle."</h1>";
												echo "<p><b>".$authorFname." ".$authorMI.". ".$authorLName."</b></p>";
												echo "<p><i>".$description."</p></i>";
											?>
							        <p><a href="#" class="btn btn-warning viewBookBtn" role="button" data-toggle="modal" data-target="#viewBookModal">View Book</a></p>
							      </div>
							    </div>
							  </div>
							</div>
						</div>
						<!-- MODAL VIEW BOOK -->
							<div class="modal fade" id="viewBookModal" tabindex="-1" role="dialog" aria-labelledby="viewBookModalLabel" aria-hidden="true">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<h5 class="modal-title" id="viewBookModalLabel">View Book</h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
											</div>
											<div class="modal-body">
											<center>
												<h3><?php echo $bookTitle;?></h3>
												Author: <p><b><?php echo $authorFname." ".$authorMI.". ".$authorLName; ?></b></p>
												Address: <p><b><?php echo $street." ".$city.", ".$state.",".$country.", ".$zip; ?></b></p>
												Year Published: <p><b><?php echo $year_pub; ?></b></p>
												Category: <p><b><?php echo $category; ?></b></p>
												<br>
												<img id="cover" src="<?php echo $bookImage?>" alt="Sample Image">
											</center>
											<br>
											<p><?php echo $description;?></p>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
										</div>
									</div>
								</div>
							</div>
							<!-- END OF MODAL VIEW BOOK -->
                        <!--reserve modal-->
                        <div class="modal fade" id="reserveModal" tabindex="-1" role="dialog" aria-labelledby="reserveModalLabel" aria-hidden="true">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<h5 class="modal-title" id="reserveModalLabel">View Book</h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
											</div>
											  <div class="modal-body">
                                                <?php
                                                foreach ($reservations as $reserveModal) {
                                                    
                                                    if($reserveModal->user_id == $studentID){
                                                        foreach ($books as $reserveBook) {
                                                            
                                                            if($reserveModal->book_id == $reserveBook->_id){
                                                                //$resImage = $reserveBook->bookInfo->image;
                                                            
                                                                echo "<h3>".$reserveBook->bookInfo->title."</h3>";
                                                                echo "Author: <p><b>".$reserveBook->contributors->authors[0]->firstName." ".$reserveBook->contributors->authors[0]->middleInitial.". ".$reserveBook->contributors->authors[0]->lastName."</b></p>";
                                                                echo "Address: <p><b>".$reserveBook->publisher->address->street.",".$reserveBook->publisher->address->city.",".$reserveBook->publisher->address->state.",".$reserveBook->publisher->address->country."</b></p>";
                                                                echo "Year Published: <p><b>".$reserveBook->publisher->yearPublished."</b></p>";
                                                                echo "Category: <p><b>".$reserveBook->category."</b></p>";
                                                                echo "<br>";
                                                                echo "<img id='cover' src='$bookImage' alt='Sample Image'>";

                                                            }
                                                        }
                                                    }
                                                }
                                                ?>
										      </div>
										<div class="modal-footer">
											<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
										</div>
									</div>
								</div>
							</div>
                        <!--end of reserve modal-->

						<div id="Reserved_Books" class="tabcontent">
                            <h3 class="tabSentences">You reserved these books:</h3>
                              <div>
                                    <table class="table table-condensed" id="reservedTable">
                                        <thead>
                                            <tr>
                                                <th>View</th>
                                                <th>Book Title</th>
                                                <th><center>Author</th>
                                                <th><center>Year</th>
                                                <th>Date Reserved</th>
                                                <th>Reservation Time</th>
                                                <th>Redeem Date</th>
                                                <th>Redeem Location</th>
                                            
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                                foreach ($reservations as $bookReservation) {
                                                    if($bookReservation->user_id == $studentID){
                                                        foreach ($books as $reservedBook) {
                                                            if($bookReservation->book_id == $reservedBook->_id){
                                                                echo "<tr>";
                                                                    echo "<td><p><a href='#' class='btn btn-warning reserveBtn' role='button' data-toggle='modal' data-target='#reserveModal'>View Book</a></p></td>";
                                                                    echo "<td>".$reservedBook->bookInfo->title."</td>";
                                                                    echo "<td><center>".$reservedBook->contributors->authors[0]->firstName." ".$reservedBook->contributors->authors[0]->middleInitial.". ".$reservedBook->contributors->authors[0]->lastName."</center></td>";
                                                                    echo "<td><center>".$reservedBook->publisher->yearPublished."</center></td>";
                                                                    echo "<td>".$bookReservation->reservationInfo->reservationDate."</td>";
                                                                    echo "<td>".$bookReservation->reservationInfo->reservationTime."</td>";
                                                                    echo "<td>".$bookReservation->redeemDate."</td>";
                                                                    echo "<td>".$bookReservation->redeemlocation."</td>";
                                                                  
                                                                
                                                                echo "</tr>";
                                                            }
                                                        }
                                                    }
                                                }
                                            ?>

                                        </tbody>
                                    </table>
                                </div>
						</div>
						</div>

						<div id="History" class="tabcontent">
							<h3 class="tabSentences">Here is a history of the books you borrowed:</h3>
							<div>
								<table class="table table-condensed" id="bookTable">
									<thead>
										<tr>
											<th>Book Title</th>
											<th><center>Author</th>
											<th><center>Year</th>
											<th>ISBN</th>
											<th>Date Borrowed</th>
											<th>Date Returned</th>
											<th>Status</th>
										</tr>
									</thead>
									<tbody>
										<?php
											foreach ($reservations as $singleReservation) {
												if($singleReservation->user_id == $studentID && $singleReservation->borrowInfo->dateReturned != null){
													foreach ($books as $singleBook) {
														// if($singleReservation->book_id == $singleBookID){
														if($singleReservation->book_id == $singleBook->_id){
															echo "<tr>";
																echo "<td>".$singleBook->bookInfo->title."</td>";
																echo "<td><center>".$singleBook->contributors->authors[0]->firstName." ".$singleBook->contributors->authors[0]->middleInitial.", ".$singleBook->contributors->authors[0]->lastName."</center></td>";
																echo "<td><center>".$singleBook->publisher->yearPublished."</center></td>";
																echo "<td>".$singleBook->bookInfo->ISBN."</td>";
																echo "<td>".$singleReservation->borrowInfo->borrowDate."</td>";
																echo "<td>".$singleReservation->borrowInfo->dateReturned."</td>";
																if($singleReservation->borrowInfo->status == "OK"){
																	echo "<td><span class='glyphicon glyphicon-thumbs-up'></span> Returned </td>";
																}else if ($singleReservation->borrowInfo->status == "PENDING") {
																	echo "<td><span class='glyphicon glyphicon-time'></span> Pending </td>";
																}else{
																	echo "<td><span class='glyphicon glyphicon-remove'></span> Overdued </td>";
																}
															echo "</tr>";
														}
													}
												}
											}
										?>

									</tbody>
								</table>
							</div>
						</div>
					</div>
					<!-- END OF TABBED PAGES -->
				</div>
				<div clas="col col-md-2"></div>
			</div>
		</div>
	</body>
	<!-- END OF HTML BODY -->

</html>
<script>
function openTab(evt, statusName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(statusName).style.display = "block";
    evt.currentTarget.className += " active";
}
</script>
<script src='datatable_files/jquery.min.js'></script>
<script src='datatable_files/datatables.min.js'></script>
<script type="text/javascript">
	$(document).ready( function () {
	    $('#bookTable').DataTable();
        $('#reservedTable').DataTable();
	} );
</script>
