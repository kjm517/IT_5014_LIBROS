<!DOCTYPE html>
<html>

<head>
    <?php include 'navbar_links.php' ?>
</head>

<body>

    <nav class="navbar navbar-expand-lg navbar-light" style="background-color: #9fedd7;">
        <img src="package/build/svg/book.svg" alt="Smiley face"> <a class="navbar-brand" href="#">&nbsp;LIBROS</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor03" aria-controls="navbarColor03" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

        <div class="collapse navbar-collapse" id="navbarColor03">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" id="aqua" href="#">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="color:#026670">
          Finance
        </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">

                        <a class="dropdown-item" href="#">View Generated Funds</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">View Unsettle Funds</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">Block Student</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">Remove Fine</a>


                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="color:#026670">
          Reservation
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          
          <a class="dropdown-item" href="reservation_Admin.php"><img src="package/build/svg/file.svg" height="15px" width="15px">&nbsp;&nbsp;View Pending</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="#"><img src="package/build/svg/check.svg" height="15px" width="15px">&nbsp;&nbsp;Accepted Requests</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="#"><img src="package/build/svg/x.svg" height="15px" width="15px">&nbsp;&nbsp;Denied Requests</a>
          
          
         
        </div>
          </li>
        <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="color:#026670">
          Book Management
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          
          <a class="dropdown-item" href="inventoryAdmin.php"><img src="package/build/svg/file.svg" height="15px" width="15px">&nbsp;&nbsp;View Books</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="addBooks.php"><img src="package/build/svg/diff-added.svg" height="15px" width="15px">&nbsp;&nbsp;Add New Book</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="#"><img src="package/build/svg/diff-removed.svg" height="15px" width="15px">&nbsp;&nbsp;Remove Books</a>
          <div class="dropdown-divider"></div>
          
          <a class="dropdown-item" href="editBooks.php"><img src="package/build/svg/pencil.svg" height="15px" width="15px">&nbsp;&nbsp;Edit Book Details</a>
        </div>
          </li>
            <div class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="color:#026670">
          Profile
        </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">

                        <a class="dropdown-item" href="admin_prof.php"><img src="package/build/svg/person.svg" height="15px" width="15px">&nbsp;&nbsp;Account Information</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#"><img src="package/build/svg/file.svg" height="15px" width="15px">&nbsp;&nbsp;View User Details</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#"><img src="package/build/svg/file.svg" height="15px" width="15px">&nbsp;&nbsp;View User List</a>
                        <div class="dropdown-divider"></div>

                        <a class="dropdown-item" href="#"><img src="package/build/svg/pencil.svg" height="15px" width="15px">&nbsp;&nbsp;Grant Admin Status</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#"><img src="package/build/svg/sign-out.svg" height="15px" width="15px">&nbsp;&nbsp;Log out</a>
                    </div>
                </li>
            </ul>

        </div>
    </nav>
</body>

</html>
