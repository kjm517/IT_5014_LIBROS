<html>
	<head>
		<?php include("links.php") ?>
    </head>
	
	<style>
		#aqua{
			color:#026670;
		}
		
		.navbar-light .navbar-nav .nav-link:focus, .navbar-light .navbar-nav .nav-link:hover {
			color: #FCE181 !important;
		} 
		
		.btn-primary {
			color: #fff;
			background-color: #0495c9;
			border-color: #357ebd; /*set the color you want here*/
		}
		
		body{
			 background-color: #EDEAE5;
		}
		#list{
			border-radius: 15px;
			background: #FFFFFF;
			width: 95%;
		}

    </style>
	
	<body>	
		<?php include("navbar_Admin.php") ?>
	<br>
	<br>

	
	<div class="container-fluid" id="List">
		<br>
		<br>
		<h3>Pending Reservations</h3>
		<hr>	
		
		<table id="reservationList" class="table table-striped table-bordered">
			<thead>
				<tr>
					<th>Reservation No</th>
					<th>Book Title</th>
					<th>Student Name</th>
					<th>Confirm</th>
					<th>Reject</th>
					<th>Reason</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>1</td>
					<td>Dummy Title</td>
					<td>Dummy Name</td>
					<td>
						<button type="button" class="btn btn-success">Confirm</button>
					</td>
					<td>
						<button type="button" class="btn btn-danger">Reject</button>
					</td>
					<td>
						<input type="text" class="form-control" id="textField">
					</td>
				</tr>
			</tbody>
		</table>
		<br>
		<br>
	</div>
	
	</body>
</html>